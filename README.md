# energix-persons-api

[OpenAPI] specification for Energix raa/2.

## Usage

### Submodules

This repository uses [git submodule](https://git-scm.com/book/nl/v2/Git-Tools-Submodules).

Execute

    > git submodule update --init

to get started 

### Private `npm` repository

Make sure you have an account on our private `npm` feed and login to it with `npm`

    > npm login
    Username: <type your feed username>
    Password: <type your feed password>
    Email: (this IS public) <type your email address>
    Logged in as <your feed username> to scope @energix on <feed URL>.

### Get the dependencies

    > npm install

### IntelliJ

We standardize on working with IntelliJ products. Project settins, in [`.idea`](.idea) are committed, so that any team
member can simply checkout the repository, execute the steps above, and have an environment that is completely set up.

## API

In [OpenAPI], reuse and separations in separate files is possible using YAML `$ref` references in some places. The root
of the specification is [`api/index.yaml`]. From here, other files in this repository are referenced directly and
indirectly.

### Development and Visualisation

Although [OpenAPI] is a standard, support for some features differs between tools. We use [ReDoc] as the main target for
our [OpenAPI] spec. [ReDoc] formats and stylizes the specification in-browser.

The tools described here provide continuous feedback on the developer's work (like unit tests).

### Linting

Linting is done with [Redocly openapi-cli].

You can execute this test locally with

    > npm test

During development, keep a console open:

    > npm run OpenAPI:watch

This will run the linter each time an OpenAPI file is changed, providing continuous feedback.

On each push, linting is done in [CI].

### Continuous developer feedback

As a developer, open [`api/index.yaml`] in a browser. This file loads [ReDoc] from a CDN (no dependencies in this
repository), and [ReDoc] loads [`api/index.yaml`] and referenced files.

If [`api/index.yaml`] contains syntax errors, [ReDoc] shows error messages and warnings.

[`api/index.yaml`] is set up to reload frequently, providing continuous feedback.

Alternatively, you can run

    > npm run OpenAPI:serve

and open your browser at the mentioned uri.

### Testing

To execute the entire batch of tests, run:

    > npm test

## Joi schemata

The data structures used in the API specification are strictly defined in [`schemata`](schemata) using [joi].

Tests are setup to check the validity of the Joi schemata. Amongst other, we validate that:

-   the schema has examples
-   all examples pass the schema validation
-   the schema fails validation as expected in a number of illegal cases

Often, schema differ slightly when used to read or write data. We use Joi's [alter / tailor] mechanism for that, with
keywords

-   `read`
-   `create`
-   `update`

These schemata are published in our private `npm` feed using semantic versioning.

## Publish

The published package contains the schemata, and a bundled version of the [OpenAPI] specification.

To publish a new version:

-   Change the version number with `> npm version (major | minor | patch)`
    ([documentation](https://docs.npmjs.com/cli/v8/commands/npm-version) ). **Always use this command, never change the
    version number by hand in `package.json`.** This avoids `package-lock.json` to get out of sync.
-   Commit this change, and push. Wait for CI to confirm that tests pass.
-   Execute `> npm publish`
-   Tag the commit with the version number. Use a nested syntax, e.g., `version/23/40/34-alpha.008`, and push the tag.

## Common

A lot of data structures and other parts of the API are common inside this service, and accross services. We are
employing consistent architectural patterns.

The common aspects of all these APIs are shared in [https://bitbucket.org/ppwcode/openapi.git].

It is included as a [git submodule](https://git-scm.com/book/nl/v2/Git-Tools-Submodules) in this repository.

[openapi]: http://spec.openapis.org/oas/v3.0.3
[ci]: ./bitbucket-pipelines.yml
[redoc]: https://github.com/Redocly/redoc
[redocly openapi-cli]: https://github.com/Redocly/openapi-cli
[`api/index.yaml`]: api/index.yaml
[`api/index.html`]: api/index.html
[joi]: https://joi.dev/
[alter / tailor]: https://joi.dev/api/?v=17.6.0#anyaltertargets
