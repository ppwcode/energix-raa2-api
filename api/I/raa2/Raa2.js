/*
 * Copyright 2021 – 2021 PeopleWare
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const Joi = require('joi')
const { Sourced, sourcedExamples } = require('./Sourced')
const { DayDate } = require('@ppwcode/openapi/time/DayDate')
const { Gender, genderExamples } = require('@ppwcode/openapi/human/Gender')
const { TrimmedString } = require('@ppwcode/openapi/string/TrimmedString')
const { Language, languageExamples } = require('@ppwcode/openapi/string/Language')
const { Address, addressExamples } = require('@ppwcode/openapi/location/Address')
const { readOnlyAlteration } = require('@ppwcode/openapi/resource/_readOnlyAlteration')
const addExamples = require('@ppwcode/openapi/_util/addExamples')

const Raa2 = Sourced.append({
    accountId: TrimmedString.required().description('account id ')
})

const raa2Examples = sourcedExamples.map(sourced => ({
    ...sourced,
    accountId: '1234',
}))

module.exports = { raa2Examples, Raa2: addExamples(Raa2, raa2Examples) }
